# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import HitOnWithEvent, TirerOnWithEvent 

class TirerOnWithAction(Action3):
	EVENT = TirerOnWithEvent
	ACTION = "tirer-on-with"
	RESOLVE_OBJECT = "resolve_for_operate"
	RESOLVE_OBJECT2 = "resolve_for_use"

class HitOnWithAction(Action3):
	EVENT = HitOnWithEvent
	ACTION = "hit-on-with"
	RESOLVE_OBJECT = "resolve_for_operate"
	RESOLVE_OBJECT2 = "resolve_for_use"
