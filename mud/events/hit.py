# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3
class TirerOnWithEvent(Event3):
    NAME = "tirer-on-with"

    def perform(self):
        if not(self.object.has_prop("full") and self.object2.has_prop("weapon")):

            self.fail()
            return self.inform("tirer-on-with.failed")

        self.inform("tirer-on-with")

class HitOnWithEvent(Event3):
    NAME = "hit-on-with"

    def perform(self):
        if not(self.object.has_prop("full") and self.object2.has_prop("weapon")):
            self.fail()
            return self.inform("hit-on-with.failed")

        self.inform("hit-on-with")
